# My Awesome Project

About ny project, this a back cli app which can monitor a give directory and back the directory when any changes are 
detected, there is also a restore function for the app.

## Getting started

To use the app, there are two modes client mode only and a server and client app.

To setup the client onlu mode you just need git clone the package from master and run the package in python3.
The App will initialize by asking the user some questions for setting things up, once this is done the app will run
till the user enters a Keyboard interrupt, which will cause the program to shutdown.

For the client/server side app you will to clone the package to both computers and run the client.py on client and 
server.py on the server, again the app on client side will ask setup question eg server ip to connect to the server. 
The server side will start and run straight away no input needed.

## Client mode only

This mode only needs to run on the client device but still requires a linux server to back up to. This mode uses SCP to 
transfer data to and from the server.

Setting up is easy, you will need to git clone the repo from the master branch.

Note you may want to setup ssh keys between the backup server and client to make it easier to communicate between them, 
but the program will do this anyway so dont worry if you cant.

Then from Project Directory run Python3 client_mode_only.py and program will start. 
The Program will ask you for:
a user name to the backup server
a password to the backup server
a IP of the backup server
a dir path of the directory you wish to monitor
a Scan Duration in seconds of how often you want the program to check for change under the directory to be monitored

To enter retrieve mode type retrieve when asked to Enter dir path, this will trigger the program to get the backed up
directory from the server. After getting the directory the program will exit.

once the program is running in normal mode and the user has entered all information, the program will run forever until 
the user enters ^C, doing this will exit the program.

## Client / Server mode

This mode runs on both the client device and backup linux server. This mode uses api requsts and a flask server to 
transfer data to and from the server.

Setting up you will need to git clone the repo from master to both client and linux server.
on the server run python3 on server.py to start the server.

Next on the client run Python3 client.py, the client side will start.
The Program will ask you for:
a IP of the backup server
a dir path of the directory you wish to monitor
a Scan Duration in seconds of how often you want the program to check for change under the directory to be monitored

To enter retrieve mode type retrieve when asked to Enter dir path, this will trigger the program to get the backed up
directory from the server. After getting the directory the program will exit.

once the program is running in normal mode and the user has entered all information, the program will run forever until 
the user enters ^C, doing this on both the server and client will exit the program.

## Test and Deploy

there are some unit test under the test directory they test the tools methods used in the background
