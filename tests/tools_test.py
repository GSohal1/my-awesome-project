from unittest import TestCase, mock
from unittest.mock import patch
from app.tools import Inputs, Diff_Engine
from pprint import pprint
import os
import paramiko
from scp import SCPClient

class test_inputs(TestCase):
    def setUp(self):
        self.Inputs = Inputs()

    def test_ask_for_input(self):
        with mock.patch('builtins.input', return_value="192.168.0.1"):
            self.assertTrue(self.Inputs.ask_for_input('Enter IP'))

    @patch('builtins.input', return_value='192.168.0.1')
    @patch('builtins.input', return_value='tests/data')
    def test_multi_question_looper(self):
        answer = self.Inputs.multi_question_looper(['Enter IP', 'Enter dir path'])
        self.assertTrue(answer)


class test_diff_engine(TestCase):
    def setUp(self):
        self.diff_engine = Diff_Engine()

    def test_check_os(self):
        if os.name == 'nt':
            self.assertIs(self.diff_engine.check_os(), 'windows')
        else:
            self.assertIs(self.diff_engine.check_os(), 'linux')

    def test_dir_search(self):
        results = self.diff_engine.dir_search('data')
        pprint(results)
        print(type(results))
        self.assertTrue(results)

    def test_dir_diff_check(self):
        current_ver_data = ['data', 'data\boo.txt', 'data\sub_boo', 'data\sub_boo\boo.txt']
        results = self.diff_engine.dir_diff_check(current_ver_data, [])

    def test_all(self):
        dir_search = self.diff_engine.dir_search('data')
        results = self.diff_engine.dir_diff_check(list(dir_search), [])

class SCP:
    def createSSHClient(server, port, user, password):
        client = paramiko.SSHClient()
        client.load_system_host_keys()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(server, port, user, password)
        return client