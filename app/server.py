from flask import Flask, request, redirect, url_for
from werkzeug import secure_filename
import os

app = Flask(__name__)

# Create a directory in a known location to save files to.
uploads_dir = os.path.join(app.instance_path, 'uploads')

@app.route('/api/', methods=['GET', 'POST'])
def store():
    # Send data back to user (retrieve mode)
    if request.method == 'POST':
        data = request.data
        # save the single "profile" file
        profile = request.files['profile']
        profile.save(os.path.join(uploads_dir, secure_filename(profile.filename)))
        return redirect(url_for('success'))

    # Receive data from client side
    if request.method == 'GET':
        folder = {'folder.zip;type', open('Storage/tmp_backup_dir', 'rb')}
        request.args.get(folder)
        return redirect(url_for('success'))


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=105)
