from tools import Inputs, Diff_Engine
from time import sleep
import requests
import shutil

print('This program will run continuously till the user enters KeyboardInterrupt, doing this will end the program.')
print('if you would like to retrieve the folder from the server, in Enter dir path type retrieve')
print('Please answer the following:')
inputs = Inputs()
diff_engine = Diff_Engine()

# Gathering requirements for Setup from User
answers = inputs.multi_question_looper(['Enter IP:', 'Enter dir path:', 'Scan Duration in seconds:'])
# Storing answers to vars, to make things easier
ip = str(answers['Enter IP'])
dir_path = str(answers['Enter dir path:'])
duration = int(answers['Scan Duration in seconds:'])

if 'retrieve' in answers['Enter dir path:']:
    response = requests.get(answers('Enter IP:'))
else:
    try:
        previous_dir_ver = ''
        while True:
            # Here the target function dir_search will look for the top of the chosen directory
            current_dir_ver = diff_engine.dir_search(dir_path)
            # This part calculates if there is a diff between the current and previous versions
            difference = diff_engine.dir_diff_check(current_dir_ver, previous_dir_ver)
            previous_dir_ver = current_dir_ver
            # Here the folder is packaged for back to be sent over https
            shutil.make_archive('Storage/tmp_backup_dir', 'zip', dir_path)
            folder = {'folder.zip;type', open('Storage/tmp_backup_dir', 'rb')}
            requests.post(ip, files=folder)
            # Wait for duration before check the directory for changes
            sleep(duration)                                                   # In seconds

    except KeyboardInterrupt:
        print('Exiting app')
        exit()

