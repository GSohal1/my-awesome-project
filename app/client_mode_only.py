from tools import Inputs, Diff_Engine, SCP
from time import sleep
import requests


print('This program will run continuously till the user enters KeyboardInterrupt, doing this will end the program.')
print('if you would like to retrieve the folder from the server, in Enter dir path type retrieve')
print('Please answer the following:')
inputs = Inputs()
diff_engine = Diff_Engine()
scp = SCP()

# Gathering requirements for Setup from User
answers = inputs.multi_question_looper(['user:', 'password:', 'Enter IP:', 'Enter dir path:', 'Scan Duration in seconds:'])
# Storing answers to vars, to make things easier
user = str(answers['user:'])
password = str(answers['password:'])
ip = str(answers['Enter IP'])
dir_path = str(answers['Enter dir path:'])
duration = int(answers['Scan Duration in seconds:'])
# Setup SCP
ssh = scp.createSSHClient(ip, '22', user, password)
scp = scp.SCPClient(ssh.get_transport())

if 'retrieve' in answers['Enter dir path:']:
    scp.get()
else:
    try:
        previous_dir_ver = ''
        while True:
            # Here the target function dir_search will look for the top of the chosen directory
            current_dir_ver = diff_engine.dir_search(dir_path)
            # This part calculates if there is a diff between the current and previous versions
            difference = diff_engine.dir_diff_check(current_dir_ver, previous_dir_ver)
            previous_dir_ver = current_dir_ver
            # Send the Folder over SCP protocol
            scp.put(dir_path)
            # Wait for duration before check the directory for changes
            sleep(duration)                                                   # In seconds

    except KeyboardInterrupt:
        print('Exiting app')
        exit()

