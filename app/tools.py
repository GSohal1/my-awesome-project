import os


class Inputs:
    def ask_for_input(self, question) -> str:
        try:
            while True:
                answers = input(question)
                if 'cancel' in answers:
                    pass
                elif answers:
                    break
            return answers
        except KeyboardInterrupt:
            print('Exiting app')
            exit()

    def multi_question_looper(self, questions) -> list:
        answers = {}
        for question in questions:
            answers.update({question: self.ask_for_input(str(question))})
            # user = self.ask_for_input('User')
            # server_ip = self.ask_for_input('Enter IP')
            # dir_path = self.ask_for_input('Enter dir path')
        return answers

class Diff_Engine:
    def check_os(self):
        """The reasoning for separating this out into a method, might need to reuses this later"""
        if os.name == 'nt':
            return 'windows'
        elif os.name == 'posix':
            return 'linux'

    def dir_search(self, dir_path) -> str:
        """Initially I was going to use a recursive search here to list directories then any sub directories
        and so on, but i found using os.walk will do this automatically"""
        # results = os.walk(dir_path, topdown=True)
        dir_contains = []
        for root, dirs, files in os.walk(dir_path, topdown=True):
            if root:
                dir_contains.append(root)
            for file in files:
                if file:
                    if self.check_os() == 'windows':
                        dir_contains.append(f'{root}\{file}')
                    else:
                        dir_contains.append(f'{root}/{file}')

        return dir_contains

    def dir_diff_check(self, current_dir_ver, previous_dir_ver):
        print(f'current_dir_ver: {type(current_dir_ver)}, {current_dir_ver}, previous_dir_ver: {type(previous_dir_ver)}, {previous_dir_ver}')
        diff = set(current_dir_ver) - set(previous_dir_ver)
        return diff
